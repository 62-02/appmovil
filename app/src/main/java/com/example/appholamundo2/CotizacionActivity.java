package com.example.appholamundo2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.appholamundo2.Cotizacion;

public class CotizacionActivity extends AppCompatActivity {
    private TextView lblNombre, lblFolio, txtDescripcion, txtValor, txtporPagoInicial, lblPagoInicial, lblPagoMensual;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private RadioGroup groupPlazos;
    private RadioButton rdb12, rdb24, rdb36, rdb48;
    private Cotizacion cotizacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        recibirDatos();
        cotizacion = (Cotizacion) getIntent().getSerializableExtra("cotizacion");
        if (cotizacion == null){
            cotizacion = new Cotizacion();
        }

        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtporPagoInicial = findViewById(R.id.txtporPagoInicial);
        txtValor = findViewById(R.id.txtValor);
        lblPagoInicial = findViewById(R.id.txtPagoInicial);
        lblPagoMensual = findViewById(R.id.txtPagoMensual);
        lblFolio = findViewById(R.id.lblFolio);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        groupPlazos = findViewById(R.id.groupPlazos);
        rdb12 = findViewById(R.id.rdb12);
        rdb24 = findViewById(R.id.rdb24);
        rdb36 = findViewById(R.id.rdb36);
        rdb48 = findViewById(R.id.rdb48);

        btnCalcular.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String descripcion = txtDescripcion.getText().toString();
                String valorStr = txtValor.getText().toString();
                String porEngancheStr = txtporPagoInicial.getText().toString();
                if (descripcion.isEmpty() || valorStr.isEmpty() || porEngancheStr.isEmpty()){
                    Toast.makeText(getApplicationContext(), "¡Favor de rellenar los datos faltantes!",Toast.LENGTH_SHORT).show();
                } else {
                    cotizacion.setDescripcion(descripcion);
                    float valor = Float.parseFloat(valorStr);
                    cotizacion.setValorAuto(valor);
                    float porcentaje = Float.parseFloat(porEngancheStr);
                    cotizacion.setPorEnganche(porcentaje);
                    int meses = 0;
                    if (rdb12.isChecked()){
                        meses = 12;
                    } else if (rdb24.isChecked()) {
                        meses = 24;
                    } else if (rdb36.isChecked()) {
                        meses = 36;
                    } else if (rdb48.isChecked()){
                        meses = 48;
                    }
                    cotizacion.setPlazos(meses);

                    // Obtener calculos
                    valor = cotizacion.calcularPagoInicial();
                    porcentaje = cotizacion.calcularPagoMensual();
                    lblPagoInicial.setText(String.format("Pago Inicial: $%.2f", valor));
                    lblPagoMensual.setText(String.format("Pago Mensual: $%.2f", porcentaje));
                }
            }
        }));
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescripcion.setText("");
                txtporPagoInicial.setText("");
                txtValor.setText("");
                lblPagoInicial.setText("Pago Inicial: ");
                lblPagoMensual.setText("Pago Mensual: ");
                groupPlazos.clearCheck();
                rdb12.setChecked(true);
            }
        });

        if (cotizacion != null){
            String info = "Folio: " + cotizacion.getFolio();
            lblFolio.setText(info);
        }
    }
    private void recibirDatos(){
        Bundle extras = getIntent().getExtras();
        String user = extras.getString("userStr");
        lblNombre = findViewById(R.id.lblNombre);
        lblNombre.setText(user);
    }
}